import React, {Component, Fragment} from 'react';
import {connect} from 'mqtt';
import * as axios from 'axios';
import MapGL, {FullscreenControl, Layer, MapContext, NavigationControl, Source} from '@urbica/react-map-gl';
import 'mapbox-gl/dist/mapbox-gl.css';

import './App.css';

class App extends Component {

    state = {
        viewport: {
            longitude: 5.835542,
            latitude: 53.051468,
            zoom: 16
        },
        devices: []
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {

        let brokerUrl;
        if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
            brokerUrl = 'ws://localhost:9001';
        } else {
            brokerUrl = `wss://mqtt.${window.location.hostname}`;
        }

        const client = connect(brokerUrl, {username: 'public', password: 'none'});

        client.on('connect', () => {
            client.subscribe('#', (err) => {
            });
        });

        client.on('message', (topic, message) => {
            if (this.map) {
                this.processData(topic, message);
            }
        });

        axios.get('/api/fleet')
            .then((response) => {
                this.setState({
                    devices: response.data.devices
                });
            });
    }

    render() {
        return (
            <MapGL
                style={{width: '100%', height: '100vh'}}
                mapStyle='mapbox://styles/mapbox/light-v10'
                accessToken='pk.eyJ1IjoiY2FzcGVya2xvcHBlbmJ1cmciLCJhIjoiY2p2amdueDNvMDRiZzN5b2MzdHltZnE3NCJ9.PIEupgtSPy1II7uR8KOCuQ'
                latitude={this.state.viewport.latitude}
                longitude={this.state.viewport.longitude}
                zoom={this.state.viewport.zoom}
                pitch={45}
                onViewportChange={viewport => this.setState({viewport})}
            >
                <FullscreenControl position='top-right'/>
                <NavigationControl showCompass showZoom position='top-right'/>

                <Layer
                    id='3d-buildings'
                    source='composite'
                    source-layer='building'
                    filter={['==', 'extrude', 'true']}
                    type='fill-extrusion'
                    minzoom={15}
                    paint={{
                        'fill-extrusion-color': '#aaa',
                        'fill-extrusion-height': [
                            'interpolate', ['linear'], ['zoom'],
                            15, 0,
                            15.05, ['get', 'height']
                        ],
                        'fill-extrusion-base': [
                            'interpolate', ['linear'], ['zoom'],
                            15, 0,
                            15.05, ['get', 'min_height']
                        ],
                        'fill-extrusion-opacity': .6
                    }}
                />

                {this.state.devices.map(this.getDeviceLayer)}

                <MapContext.Consumer>
                    {map => {
                        this.map = map;
                    }}
                </MapContext.Consumer>
            </MapGL>
        );
    }

    getDeviceLayer(device) {

        const coordinates = device.data.map(data => {
           return [data.longitude, data.latitude];
        });

        const speed = device.data[0].speed;

        return (
            <Fragment key={device.id}>
                <Source id={`history-source-${device.id}`} type='geojson' lineMetrics={true} data={{
                    type: 'FeatureCollection',
                    features: [{
                        type: 'Feature',
                        geometry: {
                            type: 'LineString',
                            properties: {},
                            coordinates
                        }
                    }]
                }}/>
                <Layer
                    id={`history-${device.id}`}
                    type='line'
                    source={`history-source-${device.id}`}
                    layout={{
                        'line-join': 'round',
                        'line-cap': 'round'
                    }}
                    paint={{
                        'line-color': '#1978c8',
                        'line-width': 8,
                        'line-gradient': [
                            'interpolate',
                            ['linear'],
                            ['line-progress'],
                            0, "rgba(25, 120, 200, 1)",
                            1, "rgba(25, 120, 200, 0)"
                        ]
                    }}
                />
                <Source id={`point-source-${device.id}`} type='geojson' data={{
                    type: 'FeatureCollection',
                    features: [{
                        type: 'Feature',
                        properties: {},
                        geometry: {
                            type: 'Point',
                            coordinates: coordinates[0]
                        }
                    }]
                }}/>
                <Layer
                    id={`point-${device.id}`}
                    type='circle'
                    source={`point-source-${device.id}`}
                    paint={{
                        'circle-radius': 6,
                        'circle-color': '#1978c8'
                    }}
                />
                <Source id={`label-source-${device.id}`} type='geojson' data={{
                    type: 'FeatureCollection',
                    features: [{
                        type: 'Feature',
                        properties: {
                            FacilityName: device.caption,
                            Comments: `${(speed * 1.852).toFixed(1)} km/h`
                        },
                        geometry: {
                            type: 'Point',
                            coordinates: coordinates[0]
                        }
                    }]
                }}/>
                <Layer
                    id={`label-${device.id}`}
                    type='symbol'
                    source={`label-source-${device.id}`}
                    layout={{
                        'text-field': ['format',
                            ['upcase', ['get', 'FacilityName']], {'font-scale': .8},
                            '\n', {},
                            ['downcase', ['get', 'Comments']], {'font-scale': .6}],
                        'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
                        'text-offset': [0, 0.6],
                        'text-anchor': 'top',
                        'text-ignore-placement': true,
                        'text-allow-overlap': true
                    }}
                />
            </Fragment>
        );
    }

    processData(topic, message) {

        const topicSegments = topic.split('/');
        if (topicSegments.length !== 2) {
            return;
        }

        const [device, type] = topicSegments;

        if (type !== 'geo') {
            return;
        }

        const dev = this.state.devices.find(d => d.name === device);
        if (!dev) {
            return;
        }

        let data;
        try {
            data = JSON.parse(message.toString());
        } catch (err) {
            return;
        }

        dev.data.unshift(data);
        if (dev.data.length > 50) {
            dev.data.pop();
        }

        const pointSource = this.map.getSource(`point-source-${dev.id}`);
        if (!pointSource) {
            return;
        }

        let [feature] = pointSource._data.features;
        feature.geometry.coordinates = [data.longitude, data.latitude];
        pointSource.setData(pointSource._data);

        const historySource = this.map.getSource(`history-source-${dev.id}`);
        if (!historySource) {
            return;
        }

        [feature] = historySource._data.features;
        feature.geometry.coordinates = dev.data.map(data => {
            return [data.longitude, data.latitude];
        });

        historySource.setData(historySource._data);

        const labelSource = this.map.getSource(`label-source-${dev.id}`);
        if (!labelSource) {
            return;
        }

        [feature] = labelSource._data.features;
        feature.properties.Comments = `${(data.speed * 1.852).toFixed(1)} km/h`;
        feature.geometry.coordinates = [data.longitude, data.latitude];
        labelSource.setData(labelSource._data);
    }
}

export default App;
