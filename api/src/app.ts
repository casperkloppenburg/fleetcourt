import 'dotenv/config';
import * as TypeOrm from 'typeorm';
import * as Redis from 'ioredis';
import * as Koa from 'koa';
import * as Router from 'koa-router';
import * as bodyParser from 'koa-bodyparser';
import * as morgan from 'koa-morgan';
import * as io from 'socket.io';
import { createServer } from 'http';

import { ENTITIES } from "./entities/entities";
import { mosquittoAclWebHook } from "./controllers/mosquitto-acl-webhook";
import { mosquittoAuthWebhook } from "./controllers/mosquitto-auth-webhook";
import { mosquittoSuperuserWebhook } from "./controllers/mosquitto-superuser-webhook";
import { getFleet } from "./controllers/get-fleet-controller";

export let redis: Redis.Redis;

(async () => {

    await TypeOrm.createConnection(
      process.env.NODE_ENV !== 'development' ? null : {
        type: 'postgres',
        host: process.env.TYPEORM_HOST,
        port: +process.env.TYPEORM_PORT,
        username: process.env.TYPEORM_USERNAME,
        password: process.env.TYPEORM_PASSWORD,
        database: process.env.TYPEORM_DATABASE,
        synchronize: true,
        entities: ENTITIES,
        // logging: true,
      }
    );

    redis = new Redis(process.env.REDIS_URI || 'redis://redis:6379');

    const koa = new Koa();

    koa.use(bodyParser({
        extendTypes: {
            json: ['application/x-javascript']
        }
    }));

    if (process.env.NODE_ENV === 'development') {
        koa.use(morgan('dev'));
    } else if (process.env.NODE_ENV !== 'test') {
        koa.use(morgan('common'));
    }

    const router = new Router();
    router.post('/private/mosquitto/auth', mosquittoAuthWebhook);
    router.post('/private/mosquitto/superuser', mosquittoSuperuserWebhook);
    router.post('/private/mosquitto/acl', mosquittoAclWebHook);
    router.get('/api/fleet', getFleet);
    koa.use(router.routes());

    const server = createServer(koa.callback());
    server.listen(4000);

    /*const socketIo = io.listen(server);
    socketIo
        .of('/data')
        .on('connection', () => {
            console.log('connected!!!');
        });*/

    console.log('Ready and listening for connections.');

})();
