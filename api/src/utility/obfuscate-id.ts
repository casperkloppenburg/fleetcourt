import * as Hashids from 'hashids';

export function ObfuscateID(salt: string): Function {
  return function (constructor: Function) {
    // Adding static Hashids field to the class.
    (<any>constructor).HASHIDS = new Hashids(salt, 6, 'abcdef0123456789');
  };
}

export function getPrivateId(publicId: string, constructor: any, onError?: () => void): number {
  const decoded = constructor.HASHIDS.decode(publicId);
  if (decoded.length !== 1 || decoded[0] == null) {
    if (onError) {
      onError();
    }
    throw new Error('Invalid public ID');
  }
  return decoded[0];
}

export function getPublicId(privateId: number | any, constructor?: any): string {
  let id: number;
  let hashids: Hashids;
  if (typeof privateId === 'number') {
    id = privateId;
    hashids = constructor.HASHIDS;
  } else {
    // In case an entity instance is provided.
    id = privateId.id;
    hashids = privateId.constructor.HASHIDS;
  }
  return hashids.encode(id);
}
