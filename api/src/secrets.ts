import * as fs from 'fs';
import * as path from 'path';

function readSecret(name: string): string {

  const filePath = path.join('/run/secrets', name);
  if (fs.existsSync(filePath)) {

    return fs.readFileSync(filePath, 'utf8');

  } else {

    const variableName = name.replace(/-/g, '_').toUpperCase();
    if (process.env[variableName] == null) {
      console.error('Missing secret ' + name + ', please provide it in /run/secrets or with environment variable ' + variableName);
    }

    return process.env[variableName];
  }
}

export const MQTT_ = readSecret('access-token-secret');
