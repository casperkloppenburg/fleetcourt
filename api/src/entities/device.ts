import { BaseEntity, Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';
import { ObfuscateID } from '../utility/obfuscate-id';

@Entity()
@ObfuscateID('f5a9b9ec-4bee-4dc4-8f06-6213dd599699')
export class Device extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Index({unique: true})
  name: string;

  @Column()
  password: string;

  @Column()
  caption: string;
}
