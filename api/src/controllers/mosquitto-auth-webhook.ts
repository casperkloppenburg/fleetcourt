import * as bcrypt from 'bcryptjs';
import * as Koa from 'koa';

import { Device } from '../entities/device';

export async function mosquittoAuthWebhook(context: Koa.Context) {

  const {username, password} = context.request.body;

  if (username !== 'public') {
    const device = await Device.findOne({name: username});
    if (!device) {
      context.throw(403);
    }

    if (!await bcrypt.compare(password, device.password)) {
      context.throw(403);
    }
  }

  context.status = 200;
}
