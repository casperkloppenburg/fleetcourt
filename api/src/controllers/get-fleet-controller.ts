import * as Koa from 'koa';

import { Device } from "../entities/device";
import { getPublicId } from "../utility/obfuscate-id";
import { redis } from "../app";

export async function getFleet(context: Koa.Context) {

    const devices = [];

    let pipeline = redis.pipeline();
    for (const device of await Device.find()) {

        devices.push({
            id: getPublicId(device),
            name: device.name,
            caption: device.caption
        });

        pipeline = pipeline.lrange(`${device.name}/geo`, 0, -1);
    }

    const data = await pipeline.exec();
    for (let i = 0; i < data.length; i++) {
        devices[i].data = data[i][1].map(JSON.parse);
    }

    context.body = {devices};
}
