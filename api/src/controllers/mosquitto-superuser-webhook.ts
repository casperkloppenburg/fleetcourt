import * as Koa from 'koa';

export async function mosquittoSuperuserWebhook(context: Koa.Context) {
  context.status = 403;
}
