import * as Koa from 'koa';

import { Device } from '../entities/device';

export async function mosquittoAclWebHook(context: Koa.Context) {

  const {username, topic, acc} = context.request.body;

  console.log(context.request.body);

  if (username === 'public') {

    // Only allow read-only access (1: MOSQ_ACL_READ) and subscriptions (4: MOSQ_ACL_SUBSCRIBE) from public connections.
    if (acc !== '1' && acc !== '4') {
      context.throw(403);
    }

  } else {

    const device = await Device.findOne({name: username});
    if (!device) {
      context.throw(403);
    }

    // Only allow write-only access (2: MOSQ_ACL_WRITE) from a device connection.
    if (acc !== '2') {
      context.throw(403);
    }

    // Devices can only publish in their own topic.
    if (!topic.startsWith(device.name + '/')) {
      context.throw(403);
    }
  }

  context.status = 200;
}
