import * as fs from 'fs';

module.exports = {
  type: 'postgres',
  host: 'postgres',
  port: 5432,
  database: 'fleetcourt',
  username: 'fleetcourt',
  password: fs.readFileSync('/run/secrets/postgres-password', 'utf8'),
  migrations: ['migrations/*.js'],
  entities: ['entities/*.js']
};
