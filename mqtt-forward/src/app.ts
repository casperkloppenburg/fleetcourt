import 'dotenv/config';
import * as mqtt from 'mqtt';
import * as Redis from 'ioredis';
import * as Influx from 'influxdb-nodejs';

(async () => {

    const mqttClient = mqtt.connect(process.env.MQTT_URI || 'ws://mosquitto:9001', {username: 'public', password: 'open'});
    const redisClient = new Redis(process.env.REDIS_URI || 'redis://redis:6379');
    const influxClient = new Influx(process.env.INFLUXDB_URI || 'http://influxdb:8086/fleetcourt');

    mqttClient.on('connect', () => {

        console.log('Connected to MQTT broker');

        mqttClient.subscribe('#', (err) => {
            if (err) {
                console.error('Failed to subscribe to topics');
                process.exit(1);
            }
        });
    });

    mqttClient.on('offline', () => {
        console.error('MQTT offline');
        process.exit(1);
    });

    mqttClient.on('message', async (topic, message) => {

        const topicSegments = topic.split('/');
        if (topicSegments.length !== 2) {
            return;
        }

        const [device, type] = topicSegments;

        let fields;
        try {
            fields = JSON.parse(message.toString());
        } catch (err) {
            return;
        }

        try {

            await redisClient
                .pipeline()
                .lpush(topic, JSON.stringify(fields))
                .ltrim(topic, 0, 49)
                .exec();

        } catch (err) {
        }

        try {

            await influxClient
                .write('akkrum')
                .tag({device, type})
                .field(fields);

        } catch (err) {
        }
    });

    redisClient.on('error', () => {
        console.error('Redis unreachable');
        process.exit(1);
    });

})();
